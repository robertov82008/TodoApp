import React from 'react';

import Menu from '../template/menu';
import Routes from './routes';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import '../template/custom.css';

export default () => (
    <div className='container'>
        <Menu />
        <Routes/>
    </div>
);